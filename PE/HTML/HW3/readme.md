## Задание

Сверстать макет [Ramda.js](https://www.figma.com/file/E4jZxsYVuNVWMrPDjqkeBC/RamdaJS2?node-id=0%3A1).

#### Технические требования к верстке

- Верстка должна быть фиксированной ширины (1280 пикселей, как в макете), весь контент должен быть расположен по центру экрана
- Лого "Ramda.js" вверху должно быть сделано текстом, в виде кликабельной ссылки
- Верхнее меню должно находиться по центру блока
- Картинка в рамочке должна быть отцентрирована горизонтально и вертикально
- Верхнее меню, блоки с боковым меню и картинкой, а также нижнее меню в футере страницы должны быть расположены горизонтально с помощью CSS свойства `display: inline-block` или `display: flex`
- В работе нужно использовать семантические теги HTML5 - header, footer, aside, main и другие
- В работе нужно использовать один из подходов - `reset.css` или `normalize.css` 

#### Необязательное задание продвинутой сложности

- Сделать чтобы пункты каждого из трех меню выделялись как на макете при наведении на них курсора мыши
- При наведении курсора мыши внутрь рамки с картинкой, увеличить немного картинку (переход должен быть плавный) - пример можно увидеть [здесь](image_scale.gif)

#### Примечение
Слово Hover на макете - это подсказка, что так выглядит элемент по наведению на него мышкой. Это не отдельный элемент, его не нужно верстать. Это примечание дизайнера.
- Верстка должна быть выполнена без использования css-библиотек типа Bootstrap или Materialize.

#### Полезные ссылки

[Сброс стилей](https://dan-it.gitlab.io/fe-book/programming_essentials/html_css/lesson4_reset_inline-block/reset_css.html)

[Контейнер в верстке](https://dan-it.gitlab.io/fe-book/programming_essentials/html_css/lesson4_reset_inline-block/container.html)

[HTML5 теги](https://dan-it.gitlab.io/fe-book/programming_essentials/html_css/lesson10_tables_html5_css3/html5.html)

[CSS hover](https://developer.mozilla.org/ru/docs/Web/CSS/:hover)


## The task

Make layout [Ramda.js](https://www.figma.com/file/E4jZxsYVuNVWMrPDjqkeBC/RamdaJS2?node-id=0%3A1).

#### Technical requirements for layout

- Layout must be fixed width (1280 pixels, as in the layout), all content must be located in the center of the screen
- The "Ramda.js" logo at the top should be made in text, as a clickable link
- The top menu should be in the center of the block
- The picture in the frame must be centered horizontally and vertically
- The top menu, sidebar and image blocks, and the bottom menu in the footer of the page should be positioned horizontally using the CSS property `display: inline-block` or `display: flex`
- You need to use HTML5 semantic tags in your work - header, footer, aside, main and others
- In the work you need to use one of the approaches - `reset.css` or `normalize.css`

#### Optional advanced task

- Make the items of each of the three menus stand out as on the layout when you hover over them with the mouse cursor
- When hovering the mouse inside the picture frame, enlarge the picture a little (the transition should be smooth) - an example can be seen [here](image_scale.gif)

#### Note
The word Hover on the layout is a hint that this is how the element looks like when hovering over it with the mouse. This is not a separate element, it does not need to be typeset. This is a designer's note.
- Layout must be done without using css libraries such as Bootstrap or Materialize.
